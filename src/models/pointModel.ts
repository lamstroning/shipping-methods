export interface PickPoint {
    address: string;
    budgets: string[];
    latitude: number;
    longitude: number;
}

export interface PickPoints {
    pickPoints: PickPoint[];
}
