import React, {createContext, useEffect, useState} from 'react';
import InteractiveMap from "./components/InteractiveMap";
import PointsList from "./components/PointsList";
import {getPoints} from "./services/map";
import {PickPoint, PickPoints} from "./models/pointModel";

export const MapContext = createContext({})

export default function App() {
    const [pickPoint, setPickPoint] = useState<PickPoint[]>([])
    const [selectedPoint, setSelectedPoint] = useState<PickPoint | undefined>()

    useEffect(() => {
        getPoints().then((result: PickPoints) =>
            setPickPoint(result.pickPoints)
        ).catch(console.error)
    }, [])


    return (
        <div className='page row'>
            <MapContext.Provider value={MapContext}>
                <PointsList pickPoints={pickPoint} onSelectPoint={setSelectedPoint}/>
                <InteractiveMap placeMarks={pickPoint} selectedPoint={selectedPoint}/>
            </MapContext.Provider>
        </div>
  );
}
