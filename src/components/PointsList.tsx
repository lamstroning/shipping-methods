import React from "react";
import {PickPoint} from "../models/pointModel";

interface ListProps {
    pickPoints: PickPoint[]
    onSelectPoint: (point: PickPoint) => void
}

export default function PointsList({pickPoints, onSelectPoint}: ListProps) {

    return (
        <div className='list'>
            <h1>Список адресов:</h1>
            {pickPoints.map(point => <button className='pick-point'
                                             key={point.latitude + point.longitude}
                                             onClick={() => onSelectPoint(point)}>
                <h2 className='pick-point__address text-overflow' title={point.address}>{point.address}</h2>

                {point.budgets.map(budget =>
                    <div
                        key={point.address + budget}
                        className='budgets'
                    >
                        {budget}
                    </div>
                )}
            </button>)}
        </div>
    )
}
