import React, {useRef} from "react";
import {YMaps, Map, Placemark} from "@pbe/react-yandex-maps";
import {PickPoint} from "../models/pointModel";

const CENTER = [55.755864, 37.617698] // Moscow

interface MapProps {
    placeMarks: PickPoint[],
    selectedPoint?: PickPoint
}

export default function InteractiveMap({placeMarks, selectedPoint}: MapProps) {
    const center = (selectedPoint && [selectedPoint.latitude, selectedPoint.longitude]) || CENTER
    const map = useRef<any>();

    map && map.current?.panTo(center)

    return (
        <div >
            <YMaps>
                <Map
                    style={{ width: '70vw', height: '100vh' }}
                    defaultState={{center, zoom: 9 }}
                    instanceRef={map}
                >
                    {placeMarks.map(({latitude, longitude}) =>
                        <Placemark key={latitude + longitude} geometry={[latitude, longitude]} />
                    )}
                </Map>
            </YMaps>
        </div>
    )
}
